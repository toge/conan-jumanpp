from conan import ConanFile
from conan.errors import ConanInvalidConfiguration
from conan.tools.files import apply_conandata_patches, export_conandata_patches, get, copy
from conan.tools.build import check_min_cppstd
from conan.tools.scm import Version
from conan.tools.cmake import CMake, CMakeDeps, CMakeToolchain, cmake_layout
import os

class JumanppConan(ConanFile):
    name = "jumanpp"
    description = "Juman++ (a Morphological Analyzer Toolkit)"
    license = "Apache-2.0"
    url = "https://bitbucket.org/toge/conan-jumanpp/"
    homepage ="https://github.com/ku-nlp/jumanpp/"
    topics = ("nlp", "japanese", "morphological-analysis", "word-segmentation")
    package_type = "library"
    settings = "os", "arch", "compiler", "build_type"
    options = {
        "shared": [True, False],
        "fPIC": [True, False],
    }
    default_options = {
        "shared": False,
        "fPIC": True,
    }

    @property
    def _min_cppstd(self):
        return 17

    # in case the project requires C++14/17/20/... the minimum compiler version should be listed
    @property
    def _compilers_minimum_version(self):
        return {
            "gcc": "7",
            "clang": "7",
            "apple-clang": "10",
        }

    def export_sources(self):
        export_conandata_patches(self)

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def configure(self):
        if self.options.shared:
            self.options.rm_safe("fPIC")

    def layout(self):
        cmake_layout(self, src_folder="src")

    def requirements(self):
        self.requires("protobuf/3.21.12")

    def validate(self):
        if self.settings.compiler.cppstd:
            check_min_cppstd(self, self._min_cppstd)
        minimum_version = self._compilers_minimum_version.get(str(self.settings.compiler), False)
        if minimum_version and Version(self.settings.compiler.version) < minimum_version:
            raise ConanInvalidConfiguration(
                f"{self.ref} requires C++{self._min_cppstd}, which your compiler does not support."
            )

    def source(self):
        get(self, **self.conan_data["sources"][self.version], strip_root=True)

#         tools.replace_in_file("jumanpp/CMakeLists.txt", "project(jumanpp)",
#                               '''project(jumanpp)
# include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
# conan_basic_setup()''')
#         tools.replace_in_file("jumanpp/src/util/common.hpp", "#define JPP_NO_INLINE __attribute__((noinline))", "#define JPP_NO_INLINE")

    def generate(self):
        tc = CMakeToolchain(self)
        tc.variables["JPP_ENABLE_TESTS"] = False
        tc.generate()
        dpes = CMakeDeps(self)
        dpes.generate()

    def build(self):
        apply_conandata_patches(self)
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        copy(self, pattern="LICENSE", dst=os.path.join(self.package_folder, "licenses"), src=self.source_folder)
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["pathie", "jpp_core_codegen", "jpp_jumandic", "jpp_jumandic_spec", "jpp_core", "jpp_util", "jpp_rnn", ]
        self.cpp_info.defines = ['JUMANPP_MODEL_FILE="{}"'.format(os.path.join(self.package_folder,"lib","jumandic.jppmdl"))]
