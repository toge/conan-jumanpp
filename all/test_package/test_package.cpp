#include <iostream>

#include "jumandic/shared/jumanpp_args.h"
#include "jumandic/shared/jumandic_env.h"

int main() {
  jumanpp::jumandic::JumanppConf conf;
  jumanpp::jumandic::JumanppExec exec{conf};
  exec.init();
}
